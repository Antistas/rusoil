<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    spl_autoload_register(function ($class_name) {
        include 'src/'. $class_name . '.php';
    });

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        Controller::getInstance()->post();
    } else {
        Controller::getInstance()->index();
    }
?>

