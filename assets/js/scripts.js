var currentOtdel = 0;

function getInfo(id) {
    currentOtdel = id;
    $.ajax('index.php?action=getSpravochnik', {
        type: 'POST',  // http method
        data: { otdel: id},  // data to submit
        success: function (data) {
            $('#result').html(data);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $('p').append('Error' + errorMessage);
        }
    });
}

function setDataToFields(modalId, data)
{
    Object.keys(data).forEach(function(k){
        $(modalId + 'Form').find('input#' + k).val(data[k]);
        if (['STATUS', 'PUBL_OUT'].includes(k)) {
            if (data[k] === 1) {
                $(modalId + 'Form').find('#' + k).prop('checked', true);
            } else {
                $(modalId + 'Form').find('#' + k).prop("checked", false);
            }
        }

        if (k === 'PARENT_OTDEL') {
            $(modalId + 'Form select#PARENT_OTDEL option[value=' + data[k] +  ']').prop('selected', true);
        }
    });
}

function addModal(type, obj) {

    if (type === 'person') {
        id = $(obj).attr('data-id-otdel');
        modalId = '#editPerson';
        $(modalId + 'Form').find('input#OTDEL').val(id);
        $(modalId + 'Form').find('input#ID').val(0);
    }

    if (type === 'otdel') {
        modalId = '#editOtdel';
        $(modalId + 'Form').find('input#OTDEL').val(0);
        $(modalId + 'Form').find('input#ID').val(0);
    }

    UIkit.modal(modalId).show();
}

function deleteObject(type, id) {

    if (confirm('Вы уверены?')) {
        $.ajax('index.php?action=delete', {
            type: 'POST',  // http method
            data: { type: type, id: id },
            success: function () {
                if (type == 'otdel') {
                    location.reload();
                } else {
                    getInfo(currentOtdel)
                }

            },
            error: function (jqXhr, textStatus, errorMessage) {
                $('p').append('Error' + errorMessage);
            }
        });
    }
}


function showModal( type,  obj ) {
    var id = 0;
    var modalId = '';

    if (type === 'person') {
        id = $(obj).attr('data-id-person');
        modalId = '#editPerson';
    }

    if (type === 'otdel') {
        id = $(obj).attr('data-id-otdel');
        modalId = '#editOtdel';
    }

    $.ajax('index.php?action=edit', {
        type: 'POST',  // http method
        data: { type: type, id: id },
        success: function (data) {
            setDataToFields(modalId, data);
            UIkit.modal(modalId).show();
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $('p').append('Error' + errorMessage);
        }
    });

    event.preventDefault();
}

function saveForm(fd, modalId)
{
    $.ajax('index.php?action=save', {
        type: 'POST',  // http method
        data: fd,  // data to submit
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.result === true) {
                UIkit.modal(modalId).hide();
                getInfo(currentOtdel)
            } else {
                alert('Произошла ошибка ' + data.message);
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $('p').append('Error' + errorMessage);
        }
    });
}

$(function() {

    $('li.dropdown').on("click", function(e){
        if ( $(this).find('ul').is(":visible") ) {
            $(this).children().not("span").hide();
        } else {
            $(this).find('ul').first().show();
        }

        getInfo($(this).attr("data-id-otdel"));

        e.stopPropagation();
        e.preventDefault();
    });

    $('li a').on("click", function(e) {
        getInfo($(this).attr("data-id-otdel"));
        e.stopPropagation();
        e.preventDefault();
    });

    $('#main_form').on('submit', function () {
        var fd = new FormData($(this)[0]);
        $.ajax('index.php?action=search', {
            type: 'POST',  // http method
            data: fd,  // data to submit
            processData: false,
            contentType: false,
            success: function (data, status, xhr) {
                $('#result').html(data);
            },
            error: function (jqXhr, textStatus, errorMessage) {
                $('p').append('Error' + errorMessage);
            }
        });
        return false;
    });

    $('#editPersonForm').on('submit', function () {
        var fd = new FormData($(this)[0]);
        fd.append('type', 'person');
        saveForm(fd, '#editPerson');
        return false;
    });

    $('#editOtdelForm').on('submit', function () {
        var fd = new FormData($(this)[0]);
        fd.append('type', 'otdel');
        saveForm(fd, '#editOtdel');
        return false;
    });
})
