<?php

class Department {

    public ?int $id;

    private array $data = [];

    function __construct($id) {
        $this->id = $id;
    }

    private static function prepareArray($result): array
    {
        $data = [];
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public static function getAll($id = null, $status = 1)
    {
        if ($id !== null) {
            $sql = <<<SQL
                    WITH RECURSIVE category_path (OTDEL, NOTDEL, PARENT_OTDEL) AS
                    (
                    SELECT OTDEL, NOTDEL, PARENT_OTDEL
                        FROM network_telephon_otdel
                        WHERE PARENT_OTDEL = $id
                      UNION ALL
                      SELECT nto.OTDEL, nto.NOTDEL, nto.PARENT_OTDEL
                        FROM category_path AS cp 
                        JOIN network_telephon_otdel AS nto
                          ON cp.OTDEL = nto.PARENT_OTDEL
                    )
                    (SELECT * FROM category_path ORDER BY OTDEL)
                    UNION
                    (SELECT OTDEL, NOTDEL, PARENT_OTDEL FROM network_telephon_otdel WHERE OTDEL = $id)
                    SQL;
        } else {
            $sql = 'SELECT * FROM network_telephon_otdel';
            if ($status !== 0) {
                $sql .= ' WHERE STATUS = '. $status;
            }
            $sql .= ' ORDER BY NOTDEL ASC, POSITION ASC';
        }

        $stmt = Connection::getInstance()->prepare($sql);
        $stmt->execute();
        return self::prepareArray($stmt->get_result());
    }


    public static function delete($id)
    {
        $depts = self::getAll($id);
        foreach ($depts as $dept) {
            $otdel = new Otdel();
            $otdel->_load($dept['OTDEL']);
            $otdel->delete();
        }
    }
}