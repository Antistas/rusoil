<?php

abstract class AbstractEntity
{
    const TABLE_NAME = '';
    const KEY_FIELD = '';

    private array $data = [];

    private static function _getVars(){
        return array_filter(get_class_vars(get_called_class()), function($elem){
            if (!is_object($elem) && !is_array($elem))
                return true;
        });
    }

    public function _load($id) {
        $sql = "SELECT * FROM " . $this::TABLE_NAME . " WHERE " . $this::KEY_FIELD . " = ? LIMIT 1";
        $stmt = Connection::getInstance()->prepare($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $this->data = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $aK = array_keys($this->data);

        foreach($aK as $sK) {
            if(!is_numeric($sK))
                $this->{$sK} = $this->data[$sK];
        }
    }

    public function delete() {
        $sql = "DELETE FROM " . $this::TABLE_NAME . " WHERE " . $this::KEY_FIELD . " = ? LIMIT 1";
        $stmt = Connection::getInstance()->prepare($sql);
        $stmt->bind_param('i', $this->{$this::KEY_FIELD});
        $stmt->execute();
    }

    private static function escape($string)
    {
        if (!is_array($string)) {
            return mysqli_real_escape_string(Connection::getInstance(), $string);
        }
    }

    private static function renderField($field)
    {
        $r = "";

        switch (gettype($field)) {
            case "integer": case "float": case "double":
                    $r = $field; break;
            case "NULL":
                $r = "NULL";
                break;
            case "boolean":
                $r = ($field) ? "true" : "false";
                break;
            case "string":
                $p_function = "/^[a-zA-Z_]+\((.)*\)/";
                preg_match($p_function, $field,$mathes);
                if (isset($mathes[0])) {
                    $p_value = "/\((.+)\)/";
                    preg_match($p_value, $field,$mValue);
                    if (isset($mValue[0]) && !empty($mValue[0])){
                        $pv = trim($mValue[0],"()");
                        $pv = "'" . self::escape($pv) . "'";
                        $r = preg_replace($p_value, "($pv)" , $field);
                    }
                    else $r = $field;
                }
                else
                    $r = "'" . self::escape($field) . "'";
                break;
            default:
                $r = "'" . self::escape($field) . "'";
                break;
        }
        return $r;
    }

    public function assign($data) {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
        return $this;
    }

    public function toArray()
    {
        $data = get_object_vars($this);

        if ($data === []) {
            $data = get_class_vars(get_called_class());
        }
        return $data;
    }

    private function execute($query)
    {
        $conn = Connection::getInstance();
        if ($conn->query($query) === TRUE) {
            return ['result' => true, 'message' => ''];
        } else {
            return ['result' => false, 'message' => 'Error updating record: ' . $conn->error];
        }
    }

    public function add(): array
    {
        $query = "INSERT INTO `" . $this::TABLE_NAME. "` (";
        $columns = self::_getVars();
        $q_column = [];
        $q_data = [];

        foreach ($columns as $k => $v){
            if ($k !== $this::KEY_FIELD) {
                $q_column[] = "`" . $k . "`";
                $q_data[] 	= self::renderField($this->$k);
            }
        }
        $query .= implode(', ', $q_column) . ') VALUES (';
        $query .= implode(', ', $q_data) . ')';

        $result = $this->execute($query);
        $insert_id = Connection::getInstance()->insert_id;

        return $result + ['insert_id' => $insert_id];
    }

    public function save()
    {
        $id = key(self::_getVars());

        if (!isset($this->$id) || empty($this->$id) || $this->$id === 0) {
            return $this->add();
        }

        $query = "UPDATE `" . $this::TABLE_NAME . "` SET ";
        $columns = self::_getVars();
        $update = [];

        foreach ($columns as $k => $v) {
            if ($id !== $k)
                $update[] = "`" . $k . "` = " . self::renderField($this->$k);
        }

        $query .= implode(", ", $update);
        $query .= " WHERE `$id` = " . self::escape($this->$id) . " LIMIT 1";

        return $this->execute($query);
    }
}