<?php

class Connection {

    private static $config = [
        'host' => 'localhost',
        'base' => 'phone_book',
        'user' => 'root',
        'pass' => ''
    ];

    protected static $instance = null;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup () {}

    public static function getInstance() {
        if(!self::$instance) {
            self::$instance = new mysqli(
                self::$config['host'],
                self::$config['user'],
                self::$config['pass'],
                self::$config['base']
            );
        }
        return self::$instance;
    }
}
?>