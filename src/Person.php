<?php

class Person extends AbstractEntity {

    public int $ID = 0;
    public int $OTDEL = 0;
    public string $KAB = '';
    public string $NDOL = '0';
    public string $TEL = '';
    public string $MANAGER = '';
    public string $OUTSIDE = '';
    public float $AUP = 0.0;
    public int $MNAGERTYPE = 0;
    public string $PEOPLE = '';
    public float $POSITION = 0.0;
    public string $EMAIL = '';
    public string $SITE = '';
    public string $WARRANT = '';
    public int $STATUS = 0;
    public string $VIEWNAME = '';
    public string $INST = '';
    public string $VACATION = '';
    public int $PUBL_OUT = 0;

    const TABLE_NAME = 'network_telephon_data';
    const KEY_FIELD = 'ID';
}