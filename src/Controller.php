<?php

declare(strict_types = 1);

class Controller {

    protected static $instance = null;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance() {
        if(!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private const ROUTES = [
        'getSpravochnik' => 'self::getSpr',
        'search' => 'self::searchData',
        'edit' => 'self::edit',
        'save' => 'self::save',
        'delete' => 'self::delete'
    ];

    private CONST IS_ADMIN = true;

    private function searchData()
    {
        $data = $_POST;
        $result = Phones::search($data['query']);
        echo $this->view('searchTable',
            [
                'data' => $result,
                'query' => "Вы искали '" . $data['query']. "'"
            ]);
    }

    private function delete() {
        $data = self::prepareInputArray($_POST);
        if ($data['type'] === 'otdel') {
            Department::delete($data['id']);
        }

        if ($data['type'] === 'person') {
            $person = new Person();
            $person->_load($data['id']);
            $person->delete();
        }
    }

    private function edit()
    {
        $data = $_POST;

        switch($data['type']) {
            case 'person': {
                $person = new Person;
                $person->_load($data['id']);
                $data = $person->toArray();
                header('Content-type: application/json');
                echo json_encode( $data );
                break;
            }
            case 'otdel': {
                $otdel = new Otdel();
                $otdel->_load($data['id']);
                $result = $otdel->toArray();

                $mainInfo = (new Phones($data['id']))->getMainInfo();
                if ($mainInfo === []) {
                    $person = (new Person())->toArray();
                } else {
                    $person = $mainInfo[0];
                }
                unset($person['OTDEL'], $person['POSITION'], $person['STATUS']);

                header('Content-type: application/json');
                $result = array_merge($result, $person);
                echo json_encode($result);
                break;
            }
        }
    }

    function prepareInputArray(array $data): array
    {
        if (!isset($data['STATUS'])) {
            $data['STATUS'] = 2;
        } else {
            $data['STATUS'] = 1;
        }

        if (!isset($data['PUBL_OUT'])) {
            $data['PUBL_OUT'] = 0;
        } else {
            $data['PUBL_OUT'] = 1;
        }

        if (isset($data['AUP'])) {
            if ($data['AUP'] === '') {
                $data['AUP'] = 0;
            }
        }

        if (isset($data['POSITION'])) {
            if ($data['POSITION'] === '') {
                $data['POSITION'] = null;
            } else {
                $data['POSITION'] = (float) $data['POSITION'];
            }
        }

        if (isset($data['ID'])) {
            $data['ID'] = (int) $data['ID'];
        }

        if (isset($data['id'])) {
            $data['id'] = (int) $data['id'];
        }

        if (isset($data['OTDEL'])) {
            $data['OTDEL'] = (int) $data['OTDEL'];
        }

        if (isset($data['otdel'])) {
            $data['otdel'] = (int) $data['otdel'];
        }

        return $data;
    }

    private function save()
    {
        if ($_POST['type'] === 'person') {
            $person = new Person;
            $data = $this->prepareInputArray($_POST);
            if ($data['ID'] > 0) {
                $person->_load($data['ID']);
            }
            $result = $person->assign($data)->save();
            header('Content-type: application/json');
            echo json_encode( $result );
        }

        if ($_POST['type'] === 'otdel') {
            $otdel = new Otdel;
            $data = $this->prepareInputArray($_POST);
            if ($data['OTDEL'] !== 0) {
                $otdel->_load($data['OTDEL']);
            }

            $result_otdel = $otdel->assign($data)->save();

            if ($result_otdel['result'] === false) {
                echo json_encode( $result_otdel );
            } else {
                unset($data['POSITION'], $data['STATUS']);
                $person = new Person;
                if ($data['ID'] !== 0) {
                    $person->_load($data['ID']);
                }
                if (isset($result_otdel['insert_id'])) {
                    $data['OTDEL'] = $result_otdel['insert_id'];
                }
                $result_person = $person->assign($data)->save();

                header('Content-type: application/json');
                if ($result_person['result'] === false) {
                    echo json_encode( $result_person );
                } else {
                    echo json_encode( ['result' => true] );
                }
            }
        }
    }

    private function getSpr()
    {
        $data = $this->prepareInputArray($_POST);

        if ($data['otdel'] !== 0) {
            $phones = new Phones($data['otdel']);
            $department = new Otdel();
            $department->_load($data['otdel']);

            if (self::IS_ADMIN) {
                $status = 0;
            } else {
                $status = 1;
            }

            $result = $phones->getPhones($status);
            $mainInfo = $phones->getMainInfo();

            echo $this->view('sprTable',
                [
                    'data' => $result,
                    'otdel' => $department,
                    'main_info' => $mainInfo,
                    'isAdmin' => self::IS_ADMIN
                ]
            );
        }
    }

    private static function getChildNode($p, $data)
    {
        $tree = [];
        foreach($data as $node) {
            if ($node['PARENT_OTDEL'] == $p) {
                $node['childs'] = self::getChildNode($node['OTDEL'], $data);
                $tree[] = $node;
            }
        }
        return $tree;
    }

    private function getDepartmentsPhonesPrint(&$depts)
    {
        foreach ($depts as &$dept) {
            $phones = new Phones($dept['OTDEL']);
            $phonesData = $phones->getPhones();
            $mainInfo = $phones->getMainInfo();
            $dept['phones'] = $phonesData;
            $dept['main_info'] = $mainInfo;
            if ($dept['childs'] !== []) {
                $this->getDepartmentsPhonesPrint($dept['childs']);
            }
        }

        return $depts;
    }

    public function print()
    {
        if (isset($_GET['otdel'])) {
            $otdel = $_GET['otdel'];
        } else {
            $otdel = null;
        }

        $depts = $this->getDepartments($otdel);
        $data = $this->getDepartmentsPhonesPrint($depts);
        echo $this->view('head');
        echo $this->view('print', ['data' => $data]);
        echo $this->view('footer');
    }

    private function getDepartments($otdel = null, $status = 1)
    {
        $data = Department::getAll($otdel, $status);
        return self::getChildNode(0, $data);
    }

    private function view($page, $variables = [])
    {
        if(count($variables))
        {
            extract($variables, EXTR_OVERWRITE);
        }
        ob_start();
        require './view/' . $page . '.php';
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

    public function post()
    {
        if (self::ROUTES[$_GET['action']] !== null) {
            $method = self::ROUTES[$_GET['action']];
            call_user_func($method);
        } else {
            echo "ERROR!";
        }
    }

    public function index()
    {

        if (self::IS_ADMIN) {
            $menu = self::getDepartments(null, 0);
            $otdels = Department::getAll(null, 0);
        } else {
            $menu = self::getDepartments();
            $otdels = [];
        }

        echo $this->view('head');
        echo $this->view('main',
            [
                'menu' => $menu,
                'otdels' => $otdels,
                'isAdmin' => self::IS_ADMIN
            ]
        );
        echo $this->view('footer');
    }
}