<?php

class Otdel extends AbstractEntity
{
    public int $OTDEL;
    public int $PARENT_OTDEL;
    public string $NOTDEL;
    public int $STATUS;
    public ?float $POSITION;

    const TABLE_NAME = 'network_telephon_otdel';
    const KEY_FIELD = 'OTDEL';


    public function delete() {

        $sql = "DELETE FROM network_telephon_data WHERE OTDEL = ?";
        $stmt = Connection::getInstance()->prepare($sql);
        $stmt->bind_param('i', $this->OTDEL);
        $stmt->execute();

        parent::delete();
    }
}