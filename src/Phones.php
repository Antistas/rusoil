<?php

class Phones {

    /*
     * id �����a
     */
    public ?int $id;

    function __construct($id) {
        $this->id = $id;
    }

    private static function prepareArray(mysqli_result $result)
    {
        $data = [];

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row = array_map(function($value) {
                    $value = trim(strip_tags($value));
                    return ($value === '-') ? '' : $value;
                }, $row
            );
            $data[] = $row;
        }

        return $data;
    }

    public static function search(string $query): array
    {
        $sql = "(SELECT * FROM network_telephon_data ntd
                LEFT JOIN network_telephon_otdel nto ON ntd.OTDEL = nto.OTDEL
                WHERE (ntd.MANAGER LIKE CONCAT('%',?,'%')) AND NDOL <> '0' 
                      ORDER BY MANAGER ASC)
                UNION
                (SELECT * FROM network_telephon_data ntd
                LEFT JOIN network_telephon_otdel nto ON ntd.OTDEL = nto.OTDEL
                WHERE (SOUNDEX(ntd.MANAGER) = SOUNDEX(?)) AND NDOL <> '0' 
                      ORDER BY MANAGER ASC)
                      ";
        $stmt = Connection::getInstance()->prepare($sql);
        $stmt->bind_param('ss', $query, $query);
        $stmt->execute();


        return self::prepareArray($stmt->get_result());
    }

    public function getMainInfo(): array
    {
        if ($this->id !== null) {
            $sql = "SELECT * FROM network_telephon_data WHERE OTDEL = ? AND NDOL = '0' LIMIT 1";
            $stmt = Connection::getInstance()->prepare($sql);
            $stmt->bind_param('i', $this->id);
            $stmt->execute();
            return $this->prepareArray($stmt->get_result());
        } else {
            return [];
        }
    }

    function getPhones($status = 1): array
    {
        if ($this->id !== null) {
            $sql = "SELECT * FROM network_telephon_data WHERE OTDEL = ? AND NDOL <> '0'";

            if ($status !== 0) {
                $sql .= ' AND STATUS = '.$status;
            }
            $sql .= " ORDER BY AUP ASC, POSITION ASC";

            $stmt = Connection::getInstance()->prepare($sql);
            $stmt->bind_param('i', $this->id);
            $stmt->execute();
            return $this->prepareArray($stmt->get_result()) ;
        } else {
            return [];
        }
    }

}

