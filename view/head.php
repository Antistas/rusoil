<!DOCTYPE html>
<html lang="ru">
<head>
    <title>RusOil</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="node_modules/uikit/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
    <script src="node_modules/uikit/dist/js/uikit.min.js"></script>
    <script src="node_modules/uikit/dist/js/uikit-icons.min.js"></script>
    <script src="node_modules/jquery/dist/jquery.js"></script>
    <script src="assets/js/scripts.js"></script>
</head>
<body>