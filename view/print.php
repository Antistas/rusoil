<div class = 'uk-container-xlarge uk-align-center'>
    <h1 class = 'uk-text-center'> Телефонный справочник УАТС УГНТУ </h1>
    <div>
        <table class="uk-table uk-table-justify uk-table-striped">
            <tbody>
            <?
                function getPhones($data) {
                    foreach ($data as $row):?>
                        <tr>
                            <td colspan = "2"><span class = "uk-text-bold"><?=$row['NOTDEL'];?></span></td>
                            <? if ($row['main_info'] !== []): ?>
                                <td><?=$row['main_info'][0]['MANAGER']. ' ' . $row['main_info'][0]['OUTSIDE']; ?></td>
                                <td><?=$row['main_info'][0]['EMAIL']; ?></td>
                                <td><?=$row['main_info'][0]['SITE']; ?></td>
                            <? else:  ?>
                                <td colspan = '3'></td>
                            <? endif; ?>
                        </tr>
                        <? foreach ($row['phones'] as $phones):?>
                            <tr>
                                <td>
                                    <? if ($phones['NDOL'] !== ''):?>
                                        <?=$phones['NDOL'].': ';?>
                                    <? endif;?>
                                    <? if ($phones['MANAGER'] !== ''):?>
                                        <?=$phones['MANAGER'];?>
                                    <? endif;?>
                                </td>
                                <td class="uk-text-nowrap"><?=$phones['TEL'];?></td>
                                <td><?=$phones['OUTSIDE'];?></td>
                                <td><?=$phones['EMAIL'];?></td>
                                <td>
                                    <? if ($phones['SITE'] !== ''): ?>
                                        <span><span class = 'person_info' uk-icon="world"></span><?=$phones['SITE']; ?></span><br />
                                    <? endif; ?>

                                    <? if ($phones['INST'] !== ''): ?>
                                        <span><span class = 'person_info' uk-icon="instagram"></span><?=$phones['INST']; ?></span>
                                    <? endif; ?>
                                </td>
                            </tr>
                        <? endforeach;?>
                        <?
                        if ($row['childs'] !== []) {
                            getPhones($row['childs']);
                        }
                        ?>
                    <? endforeach; ?>
                <?
                }
                getPhones($data);
            ?>
            </tbody>
        </table>
    </div>
</div>
