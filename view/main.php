<div class = 'uk-container-xlarge uk-align-center'>
    <h1 class = 'uk-text-center'> Телефонный справочник УАТС УГНТУ </h1>
    <div class = "uk-width-1-1@l">
        <form class="uk-form-horizontal uk-margin-medium uk-width-2-3 uk-align-center" id = "main_form">
            <div>
                <input name = "query" class="uk-input uk-display-inline-block uk-width-3-5" id="form-horizontal-text" type="text" placeholder="Введите текст поиска">
                <button class="uk-button uk-button-primary uk-display-inline-block">Найти</button>
                <a class="uk-button uk-button-default uk-display-inline-block" href = "print.php">Версия для печати</a>
            </div>
        </form>
    </div>


    <div uk-grid>
        <div class="uk-width-2-5">
            <h2 class = "uk-text-center">Содержание
                <? if ($isAdmin): ?>
                <a class = 'editButton' href="#" onclick = 'addModal("otdel", this)'>
                    <span uk-icon="plus"></span>
                </a>
                <? endif; ?>
            </h2>
            <ul class="uk-list uk-list-collapse">
                <?
                    function getMenuLevel($row) {
                        foreach ($row as $value) {

                            if ($value['STATUS'] == 1) {
                                $color = 'uk-text-emphasis';
                            } else {
                                $color = 'uk-text-muted';
                            }

                            if ($value['childs'] !== []) {
                                echo '<li class = "dropdown '.$color.'" data-id-otdel = ' . $value["OTDEL"] . '>
                                    <span uk-icon="icon: folder; ratio: .9"></span>'.$value['NOTDEL'];
                                echo '<ul class="uk-list uk-list-collapse">';
                                echo getMenuLevel($value['childs']);
                                echo '</ul></li>';
                            } else {
                                echo '<li><span class = "icon-file-text" uk-icon="icon: file-text; ratio: .9"></span>
                                    <a class = "'.$color.'" href = "#" data-id-otdel = '.$value["OTDEL"].'>'.$value["NOTDEL"].'</li></a>';
                            }
                        }
                    }

                    getMenuLevel($menu);

                ?>
            </ul>
        </div>
        <div class="uk-width-3-5" id = "result">

        </div>
    </div>
</div>

<?php if ($isAdmin): ?>
<!-- This is the modal -->
<div id="editPerson" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title"></h2>
        <form id = "editPersonForm">
            <fieldset class="uk-fieldset">
                <div class="uk-margin">
                    <label class="uk-form-label" for="MANAGER">ФИО</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="MANAGER" name = "MANAGER" type="text" placeholder="ФИО">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="NDOL">Должность</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="NDOL" name = "NDOL" type="text" placeholder="Должность">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="TEL">Внутренний телефон</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="TEL" name = "TEL" type="text" placeholder="Внутренний телефон">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="OUTSIDE">Городской телефон</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="OUTSIDE" name = "OUTSIDE" type="text" placeholder="Городской телефон">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="EMAIL">Email</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="EMAIL" name = "EMAIL" type="text" placeholder="Email">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="SITE">Сайт</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="SITE" name = "SITE" type="text" placeholder="Сайт">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="INST">Инстаграм</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="INST" name = "INST" type="text" placeholder="Инстаграм">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="KAB">Аудитория</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="KAB" name = "KAB" type="text" placeholder="Аудитория">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="AUP">АУП</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="AUP" name = "AUP" type="text" placeholder="АУП">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="WARRANT">Доверенность</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="WARRANT" name = "WARRANT" type="text" placeholder="Доверенность">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="VACATION">Отпуск</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="VACATION" name = "VACATION" type="text" placeholder="Отпуск">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="VIEWNAME">Дополнительная информация</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="VIEWNAME" name = "VIEWNAME" type="text" placeholder="Дополнительная информация">
                    </div>
                </div>

                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                    <label><input class="uk-checkbox" type="checkbox" name = "STATUS" id = "STATUS"> Отобразить</label>
                </div>
                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                    <label><input class="uk-checkbox" type="checkbox" name = "PUBL_OUT" id = "PUBL_OUT"> Публикация на официальный сайт</label>
                </div>

                <input class="uk-input" id="ID" name = "ID" type="hidden" />
                <input class="uk-input" id="OTDEL" name = "OTDEL" type="hidden" />
                <button class="uk-button uk-button-primary" type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>
</div>


<div id="editOtdel" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title"></h2>
            <form id = "editOtdelForm">
                <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="NOTDEL">Название</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="NOTDEL" name = "NOTDEL" type="text" placeholder="Название">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="PARENT_OTDEL">Родительский отдел</label>
                        <select class="uk-select" name = "PARENT_OTDEL" id = "PARENT_OTDEL">
                            <option value = '0'>Нет</option>
                            <? foreach ($otdels as $otdel): ?>
                                <option value = '<?=$otdel['OTDEL'];?>'><?=$otdel['NOTDEL'];?></option>
                            <? endforeach; ?>
                        </select>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="MANAGER">MANAGER</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="MANAGER" name = "MANAGER" type="text" placeholder="MANAGER">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="OUTSIDE">Городской телефон</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="OUTSIDE" name = "OUTSIDE" type="text" placeholder="Городской телефон">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="EMAIL">Email</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="EMAIL" name = "EMAIL" type="text" placeholder="Email">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="SITE">Сайт</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="SITE" name = "SITE" type="text" placeholder="Сайт">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="TEL">POSITION</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="POSITION" name = "POSITION" type="text" placeholder="POSITION">
                        </div>
                    </div>


                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        <label><input class="uk-checkbox" type="checkbox" name = "STATUS" id = "STATUS"> Отобразить</label>
                    </div>

                    <input class="uk-input" id="ID" name = "ID" type="hidden" />
                    <input class="uk-input" id="OTDEL" name = "OTDEL" type="hidden" />

                    <button class="uk-button uk-button-primary" type="submit">Сохранить</button>
                </fieldset>
            </form>

        </div>
    </div>
<?php endif; ?>

