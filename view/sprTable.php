<h2 class = "uk-text-center">
    <?=$otdel->NOTDEL?>
    <a href = "print.php?otdel=<?=$otdel->OTDEL?>"><span uk-icon="print"></span></a>
    <? if ($isAdmin): ?>
        <a class = 'editButton' href="#" onclick = 'showModal("otdel", this)' data-id-otdel = "<?=$otdel->OTDEL;?>">
            <span uk-icon="pencil"></span>
        </a>
        <a class = 'editButton' href="#" onclick = 'addModal("person", this)' data-id-otdel = "<?=$otdel->OTDEL;?>">
            <span uk-icon="plus"></span>
        </a>
        <a href="#" onclick = 'deleteObject("otdel", <?=$otdel->OTDEL;?>)'>
            <span uk-icon="trash"></span>
        </a>
    <? endif;?>
</h2>
<table class="uk-table uk-table-justify uk-table-striped">
    <tbody>
    <? if ($main_info !== []): ?>
    <tr class = 'main_info'>
        <td
            <? if ($isAdmin): ?>
                colspan="2"
            <?endif;?>
        >
            <? if ($main_info[0]['MANAGER'] !== ''): ?>
                <span class = "uk-text-bold"><?=$main_info[0]['MANAGER']; ?></span><br />
            <? endif; ?>
            <? if ($main_info[0]['EMAIL'] !== ''): ?>
                <span><span uk-icon="mail"></span> <?=$main_info[0]['EMAIL']; ?></span><br />
            <? endif; ?>
            <? if ($main_info[0]['SITE'] !== ''): ?>
                <span><span uk-icon="world"></span> <?=$main_info[0]['SITE']; ?></span><br />
            <? endif; ?>
        </td>
        <td colspan="2"><?=$main_info[0]['TEL']; ?></td>
        <td><?=$main_info[0]['OUTSIDE']; ?></td>
    </tr>
    <? endif; ?>
    <? foreach($data as $person): ?>
    <tr <?=($isAdmin && $person['STATUS'] == 2 ? "class = 'uk-text-muted'" : '');?>>
        <? if ($isAdmin): ?>
        <td class="uk-text-nowrap">
            <div class="uk-margin-small-left">
                <a class = 'editButton' href="#" onclick = 'showModal("person", this)' data-id-person = "<?=$person['ID'];?>">
                    <span uk-icon="pencil"></span>
                </a>
                <a class = 'editButton' href="#" onclick = 'deleteObject("person", <?=$person['ID'];?>)'>
                    <span uk-icon="trash"></span>
                </a>
            </div>

        </td>
        <? endif;?>
        <td>
            <? if ($person['NDOL'] !== ''): ?>
            <span class = "uk-text-bold"><?=$person['NDOL'];?></span><br />
            <? endif; ?>

            <? if ($person['MANAGER'] !== ''): ?>
                <span><?=$person['MANAGER']; ?></span>
                <br />
            <? endif; ?>
            <? if ($person['VACATION'] !== ''): ?>
                <span class = "uk-text-bold">Информация об отпуске:</span> <?=$person['VACATION']; ?><br />
            <? endif; ?>

            <? if ($person['EMAIL'] !== ''): ?>
            <span><span class = 'person_info' uk-icon="mail"></span><?=$person['EMAIL']; ?></span><br />
            <? endif; ?>

            <? if ($person['SITE'] !== ''): ?>
            <span><span class = 'person_info' uk-icon="world"></span><?=$person['SITE']; ?></span><br />
            <? endif; ?>

            <? if ($person['INST'] !== ''): ?>
                <span><span class = 'person_info' uk-icon="instagram"></span><?=$person['INST']; ?></span>
            <? endif; ?>
        </td>
        <td><?=$person['KAB'] ?></td>
        <td><?=$person['TEL'] ?></td>
        <td><?=$person['OUTSIDE'] ?></td>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>
