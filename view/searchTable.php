<h2 class = "uk-text-center"><?=($otdel ?? $query) ?></h2>
<table class="uk-table uk-table-small uk-table-striped">
    <tbody>
    <? $currentOtdel = ''; ?>
    <? foreach($data as $person): ?>
        <? if ($person['NOTDEL'] !== $currentOtdel): ?>
            <? $currentOtdel = $person['NOTDEL'];?>
            <tr>
                <td colspan="4" class = 'uk-text-center'><?=$person['NOTDEL'];?></td>
            </tr>
        <? endif; ?>
        <tr>
            <td>
                <? if (!in_array($person['NDOL'], ['', '-', null], true)): ?>
                    <span class = "uk-text-bold"><?=$person['NDOL'];?></span><br />
                <? endif; ?>

                <? if (!in_array($person['MANAGER'], ['', '-', null], true)): ?>
                    <span><?=$person['MANAGER']; ?></span><br />
                <? endif; ?>
                <? if (!in_array($person['EMAIL'], ['', '-', null], true)): ?>
                    <span><span class = 'person_info' uk-icon="mail"></span><?=$person['EMAIL']; ?></span><br />
                <? endif; ?>
                <? if (!in_array($person['SITE'], ['', '-', null], true)): ?>
                    <span><span class = 'person_info' uk-icon="world"></span><?=$person['SITE']; ?></span><br />
                <? endif; ?>
                <? if (!in_array($person['INST'], ['', '-', null], true)): ?>
                    <span><span class = 'person_info' uk-icon="instagram"></span><?=$person['INST']; ?></span>
                <? endif; ?>
            </td>
            <td><?=$person['KAB'] ?></td>
            <td><?=$person['TEL'] ?></td>
            <td><?=$person['OUTSIDE'] ?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>